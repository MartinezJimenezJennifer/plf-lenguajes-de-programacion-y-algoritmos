# Lenguajes de programación y algoritmos
```plantuml
@startmindmap
+[#Orange] Programando ordenadores \n en los 80 y ahora.\n ¿Qué ha cambiado? \n(2016)
++[#pink] Sistemas\n Antiguos
+++[#purple] Son sistemas descontinuados\n que existian en los años 80 hacia atras.
++++_ MS-DOS
+++[#purple] Eran programados en lenguajes de bajo nivel
++++_ Lenguaje MMaquina\n Lenguaje Ensamblador
+++[#purple] Problemas
++++[#palegreen] Limitacion de recursos hardware 
+++++[#aliceblue] Ventajas 
+++++_ Los errores en el producto\n final eran casi nulos.
+++++_ Se aprovechava el recurso\n Se trabajaba directo con el hardware
+++++[#aliceblue] Desventajas 
+++++_ Se debian conocer\n a fondo la arquitectura\n de cada computadora a programar


++[#pink] Sistemas\n Actuales
+++[#purple] Son Sistemas que se usan\n para interactuar con el sistema \n operativo de los dispositivos
++++_ Consolas de video juegos\n computadores gamer\n Sistemas Android y IOS
+++[#purple] Son sistemas programados en lenguajes de alto nivel
++++_ Java\n C\n JavaScrip\n PHP \n C++
+++[#purple] Problemas
++++_ Al tener demasiados recursos\n hace que el software se alente
++++_ El programador no conoce el hardware
++++_ Es ineficiente el uso de recursos
++++_ El producto final tiene demasiados errores
++++_ Ejecuciones tardadas
+++++[#aliceblue] Ventajas 
+++++_ Potencia de los dispositivos es mayor.
+++++_ El lenguaje usado es comprendido\n de manera facil por el programador
+++++_ Usa interpretadores y compiladores
+++++[#aliceblue] Desventajas 
+++++_ Actualizaciones y parches\n hacen ineficiente los dispositivos\n en poco tiempo
+++++_ Bug inevitables
+++++_ El tiempo de ejecucio consume tiempo
@endmindmap
```
```plantuml
@startmindmap
+[#ALICEBLUE] Historia de los algoritmos y\nde los lenguajes \nde programación \n(2010)
++[#yellow] Algoritmos
+++[#pink] Es una lista finita y ordenada\n que permite hallar la solucion a un problema\n dado un estado inicial y una entrada a traves\n de pasos sucesivos para llegar a un estado final
++++_ Son mas antiguos que los ordenadores
++++[#SeaGreen] Ejemplos
++++_ Receta de cocina, Manual de usuarios
+++[#pink] Origen El telar de Jacquard
+++[#pink] Linea del tiempo
++++_ Antigua mesopotamia se usaron para ciertos calculos\ de transacciones comerciales
++++_ siglo XVII\n Aparecen en forma de calculadoras de sobremesa
++++_ Siglo XIX\n Primeras maquinas programables
++++_ Siglo XX\n Los algoritmos alcanzan un\n desarrollo sin precedentes
+++[#pink] Limites 
++++[#SeaGreen] No todo problema sigue un algoritmo.
+++++[#red] Ejemplo
+++++_ Saber cuanta memoria va a consumir
+++[#pink] Tipos de coste (Tiempo)
++++_ Razonables \n Tiempo de ejecucion razonable
++++_ No razonables \n Tiempos Exponenciales


++[#yellow] Lenguajes de programacion
+++[#pink] Es un lenguaje de computadora\n que los programadores utilizan\n para comunicarse y para desarrollar\n programas de software, aplicaciones,\n páginas webs, scripts u otros conjuntos de\n instrucciones para que sean ejecutadas\n por los ordenadores
+++[#pink] Evolucion
++++[#SeaGreen] 1950\n Fortran Primer lenguaje de progrmacion
++++[#SeaGreen] 1960\n Llega LIPS\n Lenguaje paradigma funcional
+++++_ Capacidad para crear procesos paralelos
++++[#SeaGreen] 1968\n Aparece SIMULA\n Paradigma Orientado a Objetos
++++[#SeaGreen] 1971\n Aparece Prolog\n Surge la programacion Logica
+++++_ Se usan para problemas de optimización
+++++_ Se usan en la IA
++++[#SeaGreen] 1971\n Programas concurrentes\n Programas Paralelos
+++[#pink] Lenguajes mas Influyetes
++++_ Java
++++_ C++
@endmindmap
```


```plantuml
@startmindmap
+[#pink] Tendencias actuales\n en los lenguajes de Programación\n y Sistemas Informáticos.\n La evolución de los\n lenguajes y paradigmas\n de programación \n(2005)
++[#SeaGreen] Paradigmas
+++[#yellow] Es una forma de aproximarse a\n la solucion de un problema.
+++[#yellow] Surgen para poder solucionar errores que no pudieron ser solucionados
+++[#yellow] Programacion estructurada
++++_ Lenguajes que usan el paradigma\nC, PASCAL, FORTRAN
+++[#yellow] Paradigma Funcional
++++[#ALICEBLUE] Se basa en funciones Matematicas
++++_ Lenguajes que usan Paradigma Funcional\n ML, Haskell
+++[#yellow] Paradigma Logico 
++++[#ALICEBLUE] Se basa en expresiones logicas\n Modeliza un problema\n no tiene numeros ni letras
+++++_ Vuelve si es CIERTO o FALSO
++++_ Lenguajes que usan Paradigma Logico\n Prolog
+++[#yellow] Programacion Orientada a Objetos
++++[#ALICEBLUE] Incluye una nueva manera de\n organizar el codigo de un programa
++++[#ALICEBLUE] Se basa en el concepto clase y objetos
+++[#yellow] Programacion Basada en Componentes
++++[#ALICEBLUE] Da una reutilizacion de objetos
+++[#yellow] Programación Orientada a Aspectos 
++++[#ALICEBLUE] Permite una adecuada modularización de las aplicaciones\n y posibilita una mejor separación\n de responsabilidades \n (Obligación o correspondencia de hacer algo).

++[#SeaGreen] Tendencias
+++[#yellow] PROGRAMACION ORIENTADA A AGENTES
+++[#yellow] SISTEMAS MULTI-AGENTES
++++[#ALICEBLUE] Aplicaciones que tienen la necesidad de alcanzar sus objetivos 
++[#SeaGreen] Sistemas Informaticos
+++[#yellow] Se busca que sean mas cercanos al lenguaje natural del hombre
+++[#yellow] Se buscan que sean escalables
++[#SeaGreen] Evolucion de Leguajes
+++[#yellow] Primer Lenguaje\n 1843
+++[#yellow] Primera Generacion\n 1940
++++_ Desarrollo del lenguahe ensamblador\n 1950
+++[#yellow] Segunda Generacion\n 1950
++++_ FORTRAN\1957
++++_ LIPS?1958
++++_ COBOL\N1959
+++[#yellow] Tercera Generacion\n 1959
++++[#ALICEBLUE] Lenguajes de ALTO NIVEL
+++++_ BASIC\n 1964
+++[#yellow] Cuarta Generacion\n 1967
++++[#ALICEBLUE] PASCAL\n1970
+++[#yellow] Quienta Generacion\n 1970
++++[#ALICEBLUE] C\1972
++++[#ALICEBLUE] ADA\n 1980
++++[#ALICEBLUE] C++\1983
++++[#ALICEBLUE] Objetive-C\n 1983
++++[#ALICEBLUE] Perl\n 1987
++++[#ALICEBLUE] PHYTON\n 1991
++++[#ALICEBLUE] Ruby\n 1993
++++[#ALICEBLUE] Java\n 1995
++++[#ALICEBLUE] JavaScript\n 1995
++++[#ALICEBLUE] PHP\n 1995

@endmindmap
```
# by: Martinez Jimenez Jennifer
